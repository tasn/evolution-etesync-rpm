%define debug_package %{nil}
%define repo gitlab.gnome.org/nourmat/evolution-etesync/

Name:          evolution-etesync
Version:       0.1
Release:       1%{?dist}
Summary:       EteSync (end-to-end encrypted sync) plugin for Evolution

License:       LGPL
URL:           https://%{repo}
Source0:       https://%{repo}/-/archive/master/%{name}-master.tar.gz

Requires: libgee json-glib evolution-data-server evolution libetesync
BuildRequires: cmake meson ninja-build vala intltool evolution-data-server-devel evolution-devel libetesync

AutoReq:       no 
AutoReqProv:   no

%description
EteSync (end-to-end encrypted sync) plugin for Evolution

%prep
%setup -q -n evolution-etesync-master

%build
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DLIB_SUFFIX=64 ..
make

%install
cd build 
make DESTDIR="%{buildroot}" install

%files
%{_libdir}/evolution-etesync/libevolution-etesync.so
%{_libdir}/evolution-data-server/addressbook-backends/libebookbackendetesync.so
%{_libdir}/evolution-data-server/calendar-backends/libecalbackendetesync.so
%{_libdir}/evolution-data-server/credential-modules/module-etesync-credentials.so
%{_libdir}/evolution-data-server/registry-modules/module-etesync-backend.so
%{_libdir}/evolution/modules/module-etesync-configuration.so
%{_datadir}/metainfo/org.gnome.Evolution-etesync.metainfo.xml


%changelog
* Thu Aug 20 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 0.1-1
- First release
